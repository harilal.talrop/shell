#!/bin/bash
python manage.py makemigrations
python manage.py migrate
python manage.py loaddata initial_data user_groups permissions notification
python manage.py createsuperuser
echo "Enter port number";
read port;
python manage.py runserver $port;
