echo "Project name:" ;
read project;
echo "creating project: with project name $project";
mkdir -p $project/src;
cd $project;
python3 -m venv venv
echo "venv created..installing django"
source venv/bin/activate
pip install Django
cd src/
django-admin startproject $project
cd $project
mkdir static media templates
mkdir static/css static/js static/images
touch templates/index.html
touch static/css/style.css
touch static/js/script.js
python manage.py startapp web
open -a /Applications/Visual\ Studio\ Code.app .
open -a "Google Chrome" http://127.0.0.1:8000/ 
python manage.py runserver 
